#!/bin/bash

# Перевірка, чи встановлено Git LFS
if ! git lfs > /dev/null 2>&1; then
  echo "Git LFS не встановлено. Встановіть його за допомогою 'git lfs install'."
  exit 1
fi

# Список великих файлів, які потрібно видалити або перемістити до Git LFS
FILES=(
  "./root/model-java-script/video/video-302-javaScript-code-in-an-HTML-document.mkv"
  "./root/model-java-script/video/video-303-foundations.mp4"
  "./root/model-java-script/video/video-305-Як використовувати цикли такі як for while та dowhile.mp4"
  "./root/model-java-script/video/video-306-Як працювати з масивами та методами масивів такими як push pop s.mp4"
  "./root/model-java-script/video/video-304-if-alse.mp4"
)

# Видалення файлів з індексу
for FILE in "${FILES[@]}"; do
  if [ -f "$FILE" ]; then
    git rm --cached "$FILE"
  else
    echo "Файл $FILE не знайдено."
  fi
done

# Коміт для видалення великих файлів
git commit -m "Removed large video files from the repository"

# Додавання файлів до Git LFS
for FILE in "${FILES[@]}"; do
  git lfs track "$FILE"
done

# Додавання файлів до Git LFS і коміт
git add .gitattributes
for FILE in "${FILES[@]}"; do
  if [ -f "$FILE" ]; then
    git add "$FILE"
  fi
done
git commit -m "Moved large video files to Git LFS"

# Пуш до віддаленого репозиторію
git push origin main

echo "Операція завершена."
