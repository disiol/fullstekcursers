//Напишіть функцію на JavaScript, яка використовує оператор розповсюдження для додавання 
// кількох нових елементів у середину існуючого масиву. 
// Наприклад, функція повинна взяти масив [1, 2, 3, 4, 5], додати елементи 6, 7 після другого 
// елемента та повернути новий масив [1, 2, 6, 7, 3, 4, 5

function addElementsInMiddle(arr, index, ...newElements) {
    return [...arr.slice(0, index), ...newElements, ...arr.slice(index)];
}

const originalArray = [1, 2, 3, 4, 5];
const newArray = addElementsInMiddle(originalArray, 2, 6, 7);
console.log(newArray.toString()); // [1, 2, 6, 7, 3, 4, 5]