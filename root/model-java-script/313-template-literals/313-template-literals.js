// Практичне питання, що вимагає написання коду:
// Напишіть функцію на JavaScript, яка приймає об'єкт з інформацією про студента (ім'я, вік, спеціальність) та повертає 
// 
// рядок, сформований за допомогою шаблонного літерала, який представляє цю інформацію у зрозумілому формат

const studentInfo = (student) => `Student's Name: ${student.name}\nAge: ${student.age}\nSpecialty: ${student.specialty}`;

// Приклад використання
const student = {
    name: "Іван",
    age: 21,
    specialty: "Комп'ютерні науки"
};

console.log(studentInfo(student));
// Виведе:
// Student's Name: Іван
// Age: 21
// Specialty: Комп'ютерні науки