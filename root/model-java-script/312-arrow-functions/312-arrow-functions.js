// Напишіть стрілкову функцію, яка приймає масив чисел та повертає масив цих же чисел, піднесених до квадрату.

const squareArray = numbers => numbers.map(num => num * num);

// Приклад використання
const inputArray = [1, 2, 3, 4, 5];
const squaredArray = squareArray(inputArray);
console.log(squaredArray); // Виведе [1, 4, 9, 16, 25]