let student = {
    name: "Den", 
    age: 34,
    subjects :  ["Math", "Physics", "Chemistry", "Biology"],

    displayInfo:function (){
      console.log(`Студент ${this.name} віком  ${this.age} вивчає  ${this.subjects}`)
    }
}

student.displayInfo()